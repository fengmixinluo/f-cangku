#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>

int high,width;
int ball_x,ball_y;
int ball_vx,ball_vy;
int position_x,position_y;
int ridus;
int left,right;

void gotoxy(int x,int y)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle,pos);
}

void startup()
{
	high = 15;
	width = 20;
	ball_x = 0;
	ball_y = width/2;
	ball_vx = 1;
	ball_vy = 1;
	ridus = 5;
	position_x = high;
	position_y = width/2;
	left = position_y - ridus;
	right = position_y + ridus; 
}

void show()
{
	gotoxy(0,0);
	int i,j;
	for(i=0;i<high;i++)
	{
		for(j=0;j<width;j++)
		{
			if((i == ball_x)&&(j == ball_y))
			    printf("o");
			else if(j==width) 
			    printf("|");
			else if(i==high+1) 
			    printf("-");
			else if((i==high)&&(j>=left)&&(j<=right)) 
			    printf("*");    
			else  
			    printf(" ");		    
		}
		printf("\n");
	}
}
void updateWithoutInput()
{
	ball_x = ball_x + ball_vx;
	ball_y = ball_y + ball_vy;
	
	if((ball_x == 0)||(ball_x == high-1))
	    ball_vx = -ball_vx;
	if((ball_y == 0)||(ball_y == width-1))
	    ball_vy = -ball_vy;
		
	Sleep(50);	    
}

void updateWithInput()
{
	char input;
	if(kbhit())
	{
		input = getch();
		if(input == 'a')
		{
			position_y--;
			left = position_y - ridus;
			right = position_y + ridus;
		}
		if(input == 'd')
		{
			position_y++;
			left = position_y - ridus;
			right = position_y + ridus;
		}
	}
}

int main()
{
	startup();
	while(1)
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	return 0;
}